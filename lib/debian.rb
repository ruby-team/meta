require 'open-uri'

RMADISON_URL = 'https://api.ftp-master.debian.org/madison'

def get_rmadison_for_package(name)
  request = URI.open(RMADISON_URL + '?package=' + name, )
  retval = request.read
  #puts "#{retval}"
  hash = Hash.new()
  retval.each_line do |l|
    arr = l.split('|').map(&:strip)
    if arr[0].match(/\b#{name}/) and arr[3].match(/\bsource/)
      case arr[2]
      when 'experimental'
        hash[:experimental] = arr[1]
      when 'unstable', 'sid'
        hash[:sid] = arr[1]
      when 'testing', 'bullseye'
        hash[:bullseye] = arr[1]
      when 'stable', 'buster'
        hash[:buster] = arr[1]
      when 'oldstable', 'stretch'
        hash[:stretch] = arr[1]
      when 'oldoldstable', 'jessie'
        hash[:jessie] = arr[1]
      else
        next
      end
    end
  end
  return hash
end

# vim: set ts=2 sw=2 ai si et:
